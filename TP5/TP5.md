# I. TP5: Cloud Perso

## Sommaire

- [I. TP5: Cloud Perso](#i-TP5:-Cloud-Perso)
  - [Sommaire](#sommaire)
  - [1. Setup DB](#1-Setup-DB)
  - [2. Install MariaDB](#2-install-mariadb)
  - [3. Configuration de MariaDB](#3-Configuration-de-mariadb)
  - [4. Test](#4-test)
  - [5. Setup Web](#5-Setup-Web)
  - [6. Installation de Appache](#6-Installation-de-Appache)
  - [7. Configuration de Appache](#7-Configuration-de-Appache)
  - [8. Installation de Nextcloud](#8-Installation-de-Nextcloud)
  - [9. Test](#9-Test)

## 1. Setup DB
### 2. Installation de MariaDB
```bash=
[Eliott@db ~]$ sudo dnf install mariadb-server
[...]
Complete!
```

Service MariaDB
```bash=
[Eliott@db ~]$ sudo systemctl start mariadb.service
```

```bash=
[hugo@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
```
```bash=
[Eliott@db ~]$ systemctl status mariadb.service
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2021-11-25 11:39:01 CET; 33s ago
   [...]
```

```bash=
[Eliott@db ~]$ sudo ss -alnpt | grep sql
LISTEN 0      80                 *:3306            *:*    users:(("mysqld",pid=4583,fd=21))
```
Le numéro de port est *3306* avec comme processus *mysqld*

```bash=
[Eliott@db ~]$ ps -aux | grep sql
mysql       4583  0.0 11.1 1296832 92216 ?       Ssl  11:38   0:00 /usr/libexec/mysqld --basedir=/usr
```
Le process est lancé sous l'utilisateur *mysql*

Configuration du firewall

```bash=
[Eliott@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
[Eliott@db ~]$ sudo firewall-cmd --reload
success
```

### 3. Configuration de MariaDB

Configuration élementaire de base
```bash=
mysql_secure_installation
[...]
Enter current password for root (enter for none):
OK, successfully used password, moving on...
[...]

Set root password? [Y/n] y
#On defini un password pour l'user root pour garantir une connexion avec autorisation à cette user
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!

[...]
#On supprime la possibilité de se connecter anonymement à la DB et donc avoir un compte utilisateur pour , par exemple eviter qu'un bot se connecte avec beaucoup trop d'anonyme et surcharge la DB
Remove anonymous users? [Y/n] y
 ... Success!

[...]
#On désautorise la possibilité de se connecter à root en dehors de localhost pour éviter que le password de root soit trouvé facilement lorsque l'on se connectera à un réseau pas sécurisé
Disallow root login remotely? [Y/n] y
 ... Success!

[...]
#On supprime une database accesible par tout le monde
Remove test database and access to it? [Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

[...]
#On recharger la table des privilèges pour que les changements effectués prennent effets directement
Reload privilege tables now? [Y/n] y
 ... Success!

Cleaning up...

[...]

Thanks for using MariaDB!
```

Préparation de la base en vue de l'utilisation par NextCloud
```bash=
[Eliott@db ~]$ sudo mysql -u root -p
MariaDB [(none)]> CREATE USER 'nextcloud'@'10.5.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.001 sec)
MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.001 sec)
MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.5.1.11';
Query OK, 0 rows affected (0.001 sec)
FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```
### 4. Test
installer mysql sur web.tp5.linux
```bash=
[Eliott@web ~]$ dnf provides mysql
[...]
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7
[Eliott@web ~]$ sudo dnf install mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64
[...]
Complete!
```

Tester la connexion

```bash=
[Eliott@web ~]$ mysql --host=10.5.1.12 --port=3306 --user=nextcloud --database=nextcloud -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
[...]
mysql> SHOW TABLES
    ->
```

## 5. Setup Web

### 6. Installation de Apache
Apache
```bash=
[Eliott@web ~]$ sudo dnf install httpd
[...]
Complete!
[Eliott@web ~]$ sudo systemctl start httpd.service
[Eliott@web ~]$ systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
   Active: active (running) since Fri 2021-11-29 19:47:18 CET; 15s ago
```
Analyse du service
Pour activer le service au démarrage
```bash=
[Eliott@web ~]$ systemctl enable httpd
```
Processus lié au service
```bash=
[Eliott@web ~]$ ps -aux | grep httpd
root        2585  0.0  1.3 280220 11252 ?        Ss   09:28   0:00 /usr/sbin/httpd -DFOREGROUND
apache      2586  0.0  1.0 294104  8452 ?        S    09:28   0:00 /usr/sbin/httpd -DFOREGROUND
apache      2587  0.1  1.7 1483020 14228 ?       Sl   09:28   0:00 /usr/sbin/httpd -DFOREGROUND
apache      2588  0.1  1.4 1351892 12180 ?       Sl   09:28   0:00 /usr/sbin/httpd -DFOREGROUND
apache      2589  0.1  1.4 1351892 12180 ?       Sl   09:28   0:00 /usr/sbin/httpd -DFOREGROUND
```
Les processus sont lancés par l'utilisateur *apache*

Le port d'écoute d'Apache par défaut
```bash=
[Eliott@web ~]$ sudo ss -alnpt | grep httpd
LISTEN 0      128                *:80              *:*    users:(("httpd",pid=2589,fd=4),("httpd",pid=2588,fd=4),("http",pid=2587,fd=4),("httpd",pid=2585,fd=4))
```
Le port d'écoute par défaut est le *80*

Premier test
Ouverture du port dans le firewall
```bash=
[Eliott@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[Eliott@web ~]$ sudo firewall-cmd --reload
success
```
Test
```bash=
curl http://10.5.1.11:80
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    [...]
```

PHP

Installer php
```bash=
[Eliott@web ~]$ sudo dnf install epel-release
[...]
Complete!
[Eliott@web ~]$ sudo dnf update
[...]
Complete!
[Eliott@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
[...]
Complete!
sudo dnf module enable php:remi-7.4
[...]
Complete!
[Eliott@web ~]$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
[...]
Complete!
```

### 7. Configuration de Apache

Analyser la Configuration Apache
```bash=
[Eliott@web ~]$ cat /etc/httpd/conf/httpd.conf | grep conf
[...]
# Load config files in the "/etc/httpd/conf.d" directory, if any.
IncludeOptional conf.d/*.conf
```
Créer un VirtualHost 
```bash=
[Eliott@web conf.d]$ sudo nano nextcloud.conf
[ELiott@web conf.d]$ cat nextcloud.conf
<VirtualHost *:80>
  # on précise le dossier qui contiendra le site : la racine Web
  DocumentRoot /var/www/nextcloud/html/
```

Configurer la racine web
```bash=
[Eliott@web www]$ sudo mkdir -p nextcloud/html
[Eliott@web www]$ sudo chown apache nextcloud/
[Eliott@web www]$ sudo chown apache nextcloud/html/
```

Configurer php
```bash=
[Eliott@web www]$ timedatectl
               Local time: Fri 2021-11-26 10:27:14 CET
           Universal time: Fri 2021-11-26 09:27:14 UTC
                 RTC time: Fri 2021-11-26 09:27:12
                Time zone: Europe/Paris (CET, +0100)
                [...]
[Eliott@web ~]$ sudo nano /etc/opt/remi/php74/php.ini
[Eliott@web ~]$ cat /etc/opt/remi/php74/php.ini | grep date
[...]
date.timezone = "Europe/Paris"
[...]
```

### 8. Installation de Nextcloud

Récuperer nextcloud
```bash=
[Eliott@web ~]$ curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  148M  100  148M    0     0  15.7M      0  0:00:09  0:00:09 --:--:-- 18.9M
[Eliott@web ~]$ ls
nextcloud-21.0.1.zip
```

Ranger sa chambre
```bash=
[Eliott@web ~]$ unzip nextcloud-21.0.1.zip
[Eliott@web ~]$ sudo mv nextcloud/* /var/www/nextcloud/html/
[Eliott@web html]$ sudo chown -R apache html/
[Eliott@web ~]$ rm nextcloud-21.0.1.zip
```

### 9. Test
Modifier le fichier host du PC
```bash=
C:\Windows\System32\drivers\etc>type hosts
#
10.5.1.11 web.tp5.linux
[...]
```

Tester l'accès à son nextcloud

On se connecte à *http://web.tp5.linux*
Si c'est la première fois on dois créer un user admin et configurer la base de données comme indiqué avec les informations suivantes :
user : nextcloud avec son mot de passe
database name : nextcloud
ip avec le port de la database : 10.5.1.12:3306
