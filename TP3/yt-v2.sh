if [ -e /srv/yt/downloads/ ] && [ -e /var/log/yt ]
then 
 :
else
 exit
fi

while :
do
	link=$(head -1 /srv/yt/file.txt)
	if [[ $link != "" ]]
	then
	 title=$(youtube-dl $link -e)

	 youtube-dl -o "/srv/yt/downloads/$title/$title.mp4" $link >/dev/null
	 youtube-dl --get-description $link > /srv/yt/downloads/"$title"/description
	 echo Video $link was downloaded.
	 echo File path : /srv/yt/downloads/$title/$title.mp4
	 date=$(date +%Y/%m/%d' '%T)
	 echo $date Video $link was downloaded. File path : /srv/yt/downloads/$title/$title.mp4 >>/var/log/yt/download.log	
	 echo "$(tail -n +2 /srv/yt/file.txt)" > /srv/yt/file.txt
	else
	 echo "Waiting for link in file.txt"
	fi
	sleep 3
done
