name=$(hostname)
osname=$(hostnamectl | grep "Operating System" | cut -d":" -f2)
kernelname=$(hostnamectl | grep "Kernel" | cut -d":" -f2)
ipnb=$(ip a | grep inet | grep -v "link" | tail -n1 | cut -d"t" -f2 | cut -d" " -f2)
avaRAM=$(free -mh | grep Mem: | tr -s " " | cut -d" " -f7 | rev | cut -c 3- | rev)
totalRAM=$(free -mh | grep Mem: | tr -s " " | cut -d" " -f2 | rev | cut -c 3- | rev)
leftdisk=$(df -h / | grep sda | tr -s " " | cut -d" " -f4 | rev | cut -c 2- | rev)
processRAM=$(ps -o pid,%mem,command ax | sort -b -k2 -r | head -n6 | tail -n5 | tr -s " " | cut -d" " -f2,3,4)
nbproc=$(ss -alnpt | grep LISTEN | grep -v :: | wc -l)
urlcat=$(curl -s https://api.thecatapi.com/v1/images/search | awk -F 'http' '{print $2}' |cut -d'"' -f1)
echo Machine name : $name
echo OS $osname and Kernel version is $kernelname
echo IP : $ipnb
echo RAM : $avaRAM Go / $totalRAM Go
echo Disque : $leftdisk Go left
echo Top 5 processes by RAM usage "("PID, RAM%, Process")":
echo "$processRAM"
echo Listening ports :
for ((i = 1 ; i < $nbproc+1 ; i++));do
 port=$(ss -alnpt | grep LISTEN | grep -v :: | tr -s " " | cut -d":" -f2 | cut -d" " -f1 | head -$i | tail -1)
 nameprocess=$(ss -alnpt | grep LISTEN | grep -v :: | tr -s " " | cut -d'"' -f2 | head -$i | tail -1)
echo - $port : $nameprocess
done 
echo Here"'"s your random cat : http$urlcat
