# TP3 : A little script
## Script carte d'identité

Script carte d'identité : [carteidentite.sh](https://gitlab.com/eliottbeguerie/tplinux/-/blob/main/TP3/carteidentite.sh)
```bash=
sudo bash carteidentite.sh
```
Sortie :
```bash=
Machine name : CloneXubutuVM
OS Ubuntu 20.04.3 LTS and Kernel version is Linux 5.11.0-38-generic
IP : 192.168.56.131/24
RAM : 1,4 Go / 1,9 Go
Disque : 2,7 Go left
Top 5 processes by RAM usage (PID, RAM%, Process):
982 4.1 xfwm4
563 3.5 /usr/lib/xorg/Xorg
1096 2.2 /usr/bin/python3
1011 2.0 /usr/lib/x86_64-linux-gnu/xfce4/panel/wrapper-2.0
1018 2.0 /usr/lib/x86_64-linux-gnu/xfce4/panel/wrapper-2.0
Listening ports :
- 53 : systemd-resolve
- 22 : sshd
- 631 : cupsd
Here's your random cat : https://cdn2.thecatapi.com/images/b10.jpg
```
## Script youtube-dl

Script yt : [yt.sh](https://gitlab.com/eliottbeguerie/tplinux/-/blob/main/TP3/yt.sh)
log yt : [download.log](https://gitlab.com/eliottbeguerie/tplinux/-/blob/main/TP3/download.log)
```bash=
sudo bash yt.sh https://youtu.be/LH9yXEpoMEw
```

Sortie :
```bash=
Video https://youtu.be/LH9yXEpoMEw was downloaded.
File path : /srv/yt/downloads/Sebastian Vettel Names EVERY F1 Champion in Grill The Grid!/Sebastian Vettel Names EVERY F1 Champion in Grill The Grid!.mp4
```
## MAKE IT A SERVICE

Script yt-v2 : [yt-v2.sh](https://gitlab.com/eliottbeguerie/tplinux/-/blob/main/TP3/yt-v2.sh)
Fichier yt.service : [yt.service](https://gitlab.com/eliottbeguerie/tplinux/-/blob/main/TP3/yt.service)

En écrivant au préalable un lien youtube dans /srv/yt/file.txt

```bash=
systemctl status yt
```
Sortie :
```bash=
yt.service - YT downloader video
    Loaded: loaded (/etc/systemd/system/yt.service; disabled; vendor preset: enabled)
    Active: active (running) since Sun 2021-11-21 14:54:41 CET; 23min ago
    Main PID: 3569 (sudo)
    Tasks: 3 (limit: 2312)
    Memory: 7.5M
    CGroup: /system.slice/yt.service
            ├─3569 /usr/bin/sudo bash /srv/yt/yt-v2.sh
            ├─3570 bash /srv/yt/yt-v2.sh
            └─4798 sleep 3
            
            nov. 21 15:17:45 hugoVM sudo[3570]: Waiting for link in file.txt
            nov. 21 15:17:48 hugoVM sudo[3570]: Waiting for link in file.txt
            nov. 21 15:18:01 hugoVM sudo[3570]: Video https://youtu.be/LH9yXEpoMEw was downloaded.
            nov. 21 15:18:01 hugoVM sudo[3570]: File path : /srv/yt/downloads/Sebastian Vettel Names EVERY F1 Champion in Grill The Grid!/Sebastian Vet...
            nov. 21 15:18:07 hugoVM sudo[3570]: Waiting for link in file.txt
```

```bash=
journalctl -xe -u yt
```
Sortie :
```bash=
[...]
nov. 21 15:17:45 hugoVM sudo[3570]: Waiting for link in file.txt
nov. 21 15:17:48 hugoVM sudo[3570]: Waiting for link in file.txt
nov. 21 15:18:01 hugoVM sudo[3570]: Video https://youtu.be/LH9yXEpoMEw was downloaded.
nov. 21 15:18:01 hugoVM sudo[3570]: File path : /srv/yt/downloads/Sebastian Vettel Names EVERY F1 Champion in Grill The Grid!/Sebastian Vettel Names EVERY F1 Champion in Grill The Grid!.mp4
nov. 21 15:18:04 hugoVM sudo[3570]: Waiting for link in file.txt
[...]
```

Commande permettant le lancement du service au démarrage de la machine : 
```bash=
systemctl enable yt.service
```
