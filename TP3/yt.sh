if [ -e /srv/yt/downloads/ ] && [ -e /var/log/yt ]
then 
 :
else
 exit
fi

link=$1
title=$(youtube-dl $link -e)

youtube-dl -o "/srv/yt/downloads/$title/$title.mp4" $link >/dev/null
youtube-dl --get-description $link > /srv/yt/downloads/"$title"/description
echo Video $link was downloaded.
echo File path : /srv/yt/downloads/$title/$title.mp4
date=$(date +%Y/%m/%d' '%T)
echo $date Video $link was downloaded. File path : /srv/yt/downloads/$title/$title.mp4 >>/var/log/yt/download.log
